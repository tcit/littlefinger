defmodule Littlefinger.Result do
  alias Littlefinger.Result
  import Logger

  @mime_types_json ["application/jrd+json", "application/json"]
  @mime_types_xml ["application/xrd+xml", "application/xml", "text/xml"]

  defstruct subject: "", aliases: [], properties: [], links: []

  def parse(response) do
    mime_type = parse_content_type(response.headers)
    body = response.body

    cond do
      Enum.member?(@mime_types_json, mime_type) ->
        parse_json(body)

      Enum.member?(@mime_types_xml, mime_type) ->
        parse_xml(body)
        true
        raise Littlefinger.Error, message: "Invalid response mime type: #{mime_type}"
    end
  end

  defp parse_headers(headers, header_to_find) do
    {header_to_find, value} =
      headers
      |> Enum.filter(fn {header, _} -> header == header_to_find end)
      |> hd()

    value
  end

  defp parse_content_type(headers) do
    [content_type, _] =
      headers
      |> parse_headers("Content-Type")
      |> String.split(";")

    content_type
  end

  defp parse_json(body) do
    {:ok, json} = Poison.decode(body)

    result = %Result{
      subject: json["subject"],
      aliases: json["aliases"],
      properties: json["properties"]
    }

    parse_links(result, json["links"])
  end

  defp parse_xml(body) do
    Logger.debug(inspect(body))
  end

  defp parse_links(result, [link | tail]) do
    result = parse_links(result, tail)
    %{result | links: [link] ++ result.links}
  end

  defp parse_links(result, []) do
    result
  end
end
