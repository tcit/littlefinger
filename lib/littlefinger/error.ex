defmodule Littlefinger.Error do
  defexception message: "error"
end

defmodule Littlefinger.NotFoundError do
  defexception message: "Not found error"
end
