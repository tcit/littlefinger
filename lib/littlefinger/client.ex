defmodule Littlefinger.Client do
  @moduledoc """
  Client to perform the fetching
  """

  alias Littlefinger.Utils
  import Logger

  def finger(uri) do
    response =
      uri
      |> domain()
      |> standard_url(uri)
      |> Utils.perform_get()

    if response.status_code != 200 do
      finger_from_template(uri)
    else
      Littlefinger.Result.parse(response)
    end
  end

  def finger_from_template(url) do
    template = Utils.perform_get(url)

    if template.status_code != 200 do
      raise Littlefinger.NotFoundError, message: "No host-meta on the server"
    end

    response = Utils.perform_get(url_from_template(template.body))

    # if response.status_code != 200 do  endraise Goldfinger::NotFoundError, "No such user on the server" end

    Littlefinger.Result.parse(response)
  end

  def url_from_template(template) do
    Logger.debug(inspect(template))
    #
    #    links = xml.xpath('//xmlns:Link[@rel="lrdd"]')
    #
    #    if links.empty? do raise Goldfinger::NotFoundError end
    #
    #    links.first.attribute('template').value.gsub('{uri}', @uri)
    #    rescue Nokogiri::XML::XPath::SyntaxError
    #    raise Goldfinger::Error, "Bad XML: #{template}"
  end

  defp standard_url(domain, uri) do
    "https://#{domain}/.well-known/webfinger?resource=#{uri}"
  end

  defp url(domain) do
    "https://#{domain}/.well-known/host-meta"
  end

  defp domain(uri) do
    [_, domain] = uri |> String.split("@")
    domain
  end
end
