defmodule Littlefinger.Utils do
  def perform_get(path, headers \\ [], options \\ []) do
    perform_request(:get, path, headers, options)
  end

  def perform_request(request_method, path, headers \\ [], options \\ []) do
    Littlefinger.Request.perform(request_method, path, headers, options)
  end
end
