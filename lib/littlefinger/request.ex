defmodule Littlefinger.Request do
  @moduledoc """
  Perform a request
  """
  import Logger

  def perform(request_method, uri, headers \\ [], options \\ []) do
    case request_method do
      :get ->
        {:ok, response} = HTTPoison.get(uri)
    end

    response
  end
end
