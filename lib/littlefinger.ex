defmodule Littlefinger do
  @moduledoc """
  Documentation for Littlefinger.
  """

  def finger(uri) do
    Littlefinger.Client.finger(uri)
  end
end
