defmodule Littlefinger.MixProject do
  use Mix.Project

  def project do
    [
      app: :littlefinger,
      version: "0.1.0",
      elixir: "~> 1.6",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      name: "Littlefinger",
      source_url: "https://framagit.org/tcit/littlefinger"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.0"},
      {:poison, "~> 3.1"},
      {:ex_doc, "~> 0.16", only: :dev, runtime: false}
    ]
  end

  defp description() do
    "Performs Webfinger requests to discover information on people or other entities identified through IRIs."
  end

  defp package() do
    [
      # This option is only needed when you don't want to use the OTP application name
      name: "littlefinger",
      # These are the default files included in the package
      files: ["lib", "mix.exs", "README*", "LICENSE*"],
      maintainers: ["Thomas Citharel"],
      licenses: ["AGPL-3.0"],
      links: %{"Framagit" => "https://framagit.org/tcit/littlefinger"}
    ]
  end
end
