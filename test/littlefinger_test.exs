defmodule LittlefingerTest do
  use ExUnit.Case
  doctest Littlefinger

  test "Calls simple url" do
    assert Littlefinger.finger("tcit@social.tcit.fr") == %Littlefinger.Result{
             aliases: ["https://social.tcit.fr/@tcit", "https://social.tcit.fr/users/tcit"],
             links: [
               %{
                 "href" => "https://social.tcit.fr/@tcit",
                 "rel" => "http://webfinger.net/rel/profile-page",
                 "type" => "text/html"
               },
               %{
                 "href" => "https://social.tcit.fr/users/tcit.atom",
                 "rel" => "http://schemas.google.com/g/2010#updates-from",
                 "type" => "application/atom+xml"
               },
               %{
                 "href" => "https://social.tcit.fr/users/tcit",
                 "rel" => "self",
                 "type" => "application/activity+json"
               },
               %{
                 "href" => "https://social.tcit.fr/api/salmon/1",
                 "rel" => "salmon"
               },
               %{
                 "href" =>
                   "data:application/magic-public-key,RSA.pXwYMUdFg3XUd-bGsh8CyiMRGpRGAWuCdM5pDWx5uM4pW2pM3xbHbcI21j9h8BmlAiPg6hbZD73KGly2N8Rt5iIS0I-l6i8kA1JCCdlAaDTRd41RKMggZDoQvjVZQtsyE1VzMeU2kbqqTFN6ew7Hvbd6O0NhixoKoZ5f3jwuBDZoT0p1TAcaMdmG8oqHD97isizkDnRn8cOBA6wtI-xb5xP2zxZMsLpTDZLiKU8XcPKZCw4OfQfmDmKkHtrFb77jCAQj_s_FxjVnvxRwmfhNnWy0D-LUV_g63nHh_b5zXIeV92QZLvDYbgbezmzUzv9UeA1s70GGbaDqCIy85gw9-w==.AQAB",
                 "rel" => "magic-public-key"
               },
               %{
                 "rel" => "http://ostatus.org/schema/1.0/subscribe",
                 "template" => "https://social.tcit.fr/authorize_follow?acct={uri}"
               }
             ],
             properties: nil,
             subject: "acct:tcit@social.tcit.fr"
           }
  end
end
